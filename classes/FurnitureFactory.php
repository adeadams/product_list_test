<?php
class FurnitureFactory implements ProductFactory
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function createProduct()
    {
        return new Furniture($this->data);
    }
}
