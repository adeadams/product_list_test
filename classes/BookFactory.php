<?php
class BookFactory implements ProductFactory
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function createProduct()
    {
        return new Book($this->data);
    }
}
