<?php
//this file declares symbols (classes, functions, constants
class Furniture implements Product
{
    public function addToDb($data)
    {
        return addToDatabase($data);
    }

    public function formatPd($data)
    {
        function formatSpec($data)
        {
            $len = $data['length'];
            $wid = $data['height'];
            $hgt = $data['width'];
            $res = trim(sprintf("Dimensions: %sx%sx%s", $len, $wid, $hgt));
            return $res;
        }

        $b = [];
        $dims = formatSpec($data);
        $price = formatPrice($data['price']);
        $b['sku'] = $data['sku'];
        $b['name'] = $data['name'];
        $b['price'] = $price;
        $b['type'] = $data['product_type'];
        $b['spec'] = $dims;
        return $b;
    }
}
