<?php
//this file declares symbols (classes, functions, constants

include('../interfaces/ProductFactory.php');
include('../interfaces/Product.php');
include('../utils/addtoDb.php');
class Book implements Product
{
    public function addToDb($data)
    {
        return addToDatabase($data);
    }

    public function formatPd($data)
    {
        function formatSpec($data)
        {
            $res = trim(sprintf("Weight: %sKg", $data));
            return $res;
        }

        $b = [];
        $weight = formatSpec($data['weight']);
        $price = formatPrice($data['price']);
        $b['sku'] = $data['sku'];
        $b['name'] = $data['name'];
        $b['price'] = $price;
        $b['type'] = $data['product_type'];
        $b['spec'] = $weight;
        return $b;
    }
}
