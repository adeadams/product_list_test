<?php
class DVDFactory implements ProductFactory
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function createProduct()
    {
        return new DVD($this->data);
    }
}
