<?php
//this file declares symbols (classes, functions, constants
class DVD implements Product
{
    public function addToDb($data)
    {
        return addToDatabase($data);
    }

    public function formatPd($data)
    {
        function formatSpec($data)
        {
            $res = trim(sprintf("Size: %sMB", $data));
            return $res;
        }

        $b = [];
        $size = formatSpec($data['size']);
        $price = formatPrice($data['price']);
        $b['sku'] = $data['sku'];
        $b['name'] = $data['name'];
        $b['price'] = $price;
        $b['type'] = $data['product_type'];
        $b['spec'] = $size;
        return $b;
    }
}
