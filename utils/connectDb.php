<?php
function connect()
{
    // changed mysqli to object-oriented
    $host = "localhost";
    $dbUser = "ade";
    $password = "ade123";
    $database = "products_db";

    $connection = new mysqli($host, $dbUser, $password, $database);

    if ($connection->connect_error) {
        die("Database Connection Error, Error No.: " . $connection->connect_errno . " | " . $connection->connect_error);
    }
    return $connection;
}
