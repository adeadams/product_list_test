<?php

include('./utils/connectDb.php');

function getAll()
{
    $conn = connect();
    // get products from db
    $db_fetch = $conn->prepare("CALL getAllProducts()");
    $db_fetch->execute();
    $raw = $db_fetch->get_result();
    $arrs = $raw->fetch_all(MYSQLI_ASSOC);

    $prods = json_encode($arrs);
    print_r($prods);
}
