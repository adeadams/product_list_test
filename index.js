var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function () {
  if (this.readyState == 4 && this.status == 200) {
    const result = this.responseText;
    const arr = JSON.parse(result);

    // loop through merged arr into html template
    const html = arr.map(prods => {
      const html = `
          <div id="t" class="col-md-3">
            <div class="card">
                    <div class="card-header">
                        <div class="form-check">
                            <input class="form-check-input delete-checkbox" type="checkbox" value=${prods.sku} id=""> 
                            ${prods.product_type}
                        </div>
                    </div>
                    <div class="card-body">
                        <ul id="spec" class="list-group list-group-flush">
                            <li class="list-group-item">${prods.sku}</li>
                            <li class="list-group-item">${prods.name}</li>
                            <li class="list-group-item">$${prods.price}.00</li>
                            <li class="list-group-item">${prods.product_specific}</li>
                        </ul>
                    </div> 
                  </div>
                </div>
               `;
      return html;
    });
    // set returned template as inner html
    document.getElementById("t").innerHTML = html;
  }
};

xmlhttp.open("GET", "index.php?q=", true);

xmlhttp.send();
